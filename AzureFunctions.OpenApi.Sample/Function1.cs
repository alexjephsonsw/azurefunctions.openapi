using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;

namespace AzureFunctions.OpenApi.Sample
{
    public class SampleFunction
    {
        public class Model
        {
            public string Description { get; set; }
            public int Id { get; set; }
        }

        [FunctionName("Get-Sample")]
        public async Task<IActionResult> Get(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "ham/{nice}")] string nice)
        {
            return new OkObjectResult(new
            {
                Name = nice,
            });
        }

        [FunctionName("Post-Sample")]
        public async Task<IActionResult> Post(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "ham")] Model model)
        {
            return new OkObjectResult(model);
        }
    }
}