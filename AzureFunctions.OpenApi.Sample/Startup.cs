﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

[assembly: FunctionsStartup(typeof(AzureFunctions.OpenApi.Sample.Startup))]

namespace AzureFunctions.OpenApi.Sample
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            var services = builder.Services;

            //services.Replace<IVersionGenerator, CustomVersionGenerator>();

            services.AddOptions<OpenApiOptions>().Configure<IConfiguration>((options, configuration) =>
            {
                options.Title = configuration.GetValue<string>("Documentation:Title");
            });
        }
    }
}