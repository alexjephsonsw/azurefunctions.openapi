﻿using AzureFunctions.OpenApi.Binding;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Writers;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace AzureFunctions.OpenApi.Sample
{
    public class SwaggerFunction
    {
        [FunctionName("swagger-json")]
        public Task<IActionResult> GetJson([HttpTrigger(Route = "swagger.json")] HttpRequestMessage request, [OpenApi] OpenApiDocument doc)
        {
            try
            {
                using (var sw = new StringWriter())
                {
                    var writer = new OpenApiJsonWriter(sw);

                    doc.SerializeAsV3(writer);

                    return Task.FromResult<IActionResult>(new OkObjectResult(sw.ToString()));
                }
            }
            catch (Exception ex)
            {
                return Task.FromResult<IActionResult>(new ObjectResult(ex.Message) { StatusCode = 500 });
            }
        }

        [FunctionName("swagger-yaml")]
        public Task<IActionResult> GetYaml([HttpTrigger(Route = "swagger.yaml")] object obj, [OpenApi] OpenApiDocument doc)
        {
            using (var sw = new StringWriter())
            {
                var writer = new OpenApiYamlWriter(sw);

                doc.SerializeAsV3(writer);

                return Task.FromResult<IActionResult>(new OkObjectResult(sw.ToString()));
            }
        }
    }
}