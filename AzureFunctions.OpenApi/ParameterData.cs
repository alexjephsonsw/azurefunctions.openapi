﻿using Microsoft.OpenApi.Models;
using System;

namespace AzureFunctions.OpenApi
{
    /// <summary>
    ///
    /// </summary>
    public class ParameterData
    {
        /// <summary>
        ///
        /// </summary>
        public Type Type { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public bool Required { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public ParameterLocation? ParameterLocation { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public ParameterStyle? Style { get; internal set; }
    }
}