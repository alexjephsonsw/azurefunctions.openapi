﻿using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.DependencyInjection;
using AzureFunctions.OpenApi;
using AzureFunctions.OpenApi.Binding;
using AzureFunctions.OpenApi.Generator;

[assembly: WebJobsStartup(typeof(OpenApiStartup))]

namespace AzureFunctions.OpenApi
{
    public class OpenApiStartup : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            var services = builder.Services;

            services.AddSingleton<ITypeHandler, DefaultTypeHandler>();

            services.AddSingleton<ITypeHandlerFactory, TypeHandlerFactory>();
            services.AddSingleton<ITypeIdGenerator, TypeIdGenerator>();
            services.AddSingleton<IParameterGenerator, ParameterGenerator>();
            services.AddSingleton<IParametersGenerator, ParametersGenerator>();
            services.AddSingleton<IMethodMapper, MethodMapper>();
            services.AddSingleton<IOperationIdGenerator, OperationIdGenerator>();
            services.AddSingleton<IResponsesGenerator, ResponsesGenerator>();
            services.AddSingleton<IRouteGenerator, RouteGenerator>();
            services.AddSingleton<ISampleContentGenerator, DefaultSampleContentGenerator>();
            services.AddSingleton<IVersionGenerator, AssemblyVersionGenerator>();
            services.AddSingleton<IMetadataGenerator, MetadataGenerator>();
            services.AddSingleton<IApiInfoGenerator, DefaultApiInfoGenerator>();
            services.AddSingleton<IPathsGenerator, PathsGenerator>();
            services.AddSingleton<IServersGenerator, DefaultServersGenerator>();
            services.AddSingleton<ITagsGenerator, DefaultTagsGenerator>();
            services.AddSingleton<ISecurityGenerator, SecurityGenerator>();
            services.AddSingleton<IOpenApiDocumentGenerator, OpenApiDocumentGenerator>();

            builder.AddExtension<OpenApiConfigProvider>();
        }
    }
}