﻿using System;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi
{
    /// <summary>
    ///
    /// </summary>
    public class OpenApiOptions
    {
        /// <summary>
        /// The supported content types. This is used to generate sample content
        /// </summary>
        public IEnumerable<string> ContentTypes { get; set; } = Array.Empty<string>();

        /// <summary>
        /// The title of the documentation
        /// </summary>
        public string Title { get; set; } = "OpenApi Title";

        /// <summary>
        /// Overrides the default version generation with a specific value
        /// </summary>
        public string Version { get; set; } = null;

        /// <summary>
        /// The description of the documentation
        /// </summary>
        public string Description { get; set; } = "OpenApi Description";
    }
}