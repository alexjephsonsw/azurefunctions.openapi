﻿using AzureFunctions.OpenApi.Generator;
using Microsoft.Azure.WebJobs.Host.Config;

namespace AzureFunctions.OpenApi.Binding
{
    internal class OpenApiConfigProvider : IExtensionConfigProvider
    {
        private readonly IOpenApiDocumentGenerator _openApiDocumentGenerator;

        public OpenApiConfigProvider(IOpenApiDocumentGenerator openApiDocumentGenerator)
        {
            _openApiDocumentGenerator = openApiDocumentGenerator;
        }

        public void Initialize(ExtensionConfigContext context)
        {
            var bindingProvider = new OpenApiBindingProvider(_openApiDocumentGenerator);
            context.AddBindingRule<OpenApiAttribute>()
                .Bind(bindingProvider);
        }
    }
}