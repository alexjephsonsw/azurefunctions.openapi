﻿using AzureFunctions.OpenApi.Generator;
using Microsoft.Azure.WebJobs.Host.Bindings;
using Microsoft.Azure.WebJobs.Host.Protocols;
using Microsoft.OpenApi.Models;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace AzureFunctions.OpenApi.Binding
{
    internal class OpenApiBindingProvider : IBindingProvider
    {
        private readonly IOpenApiDocumentGenerator _openApiDocumentGenerator;

        private OpenApiDocument _document;

        public OpenApiBindingProvider(IOpenApiDocumentGenerator openApiDocumentGenerator)
        {
            _openApiDocumentGenerator = openApiDocumentGenerator;
        }

        public Task<IBinding> TryCreateAsync(BindingProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var parameter = context.Parameter;
            var attribute = parameter.GetCustomAttribute<OpenApiAttribute>(inherit: false);
            if (attribute == null)
            {
                return Task.FromResult<IBinding>(null);
            }

            if (parameter.ParameterType != typeof(OpenApiDocument))
            {
                throw new InvalidOperationException("Cannot bind OpenApi attribute to a type other than OpenApiDocument");
            }

            if (_document == null)
            {
                _document = _openApiDocumentGenerator.Generate(parameter.Member.DeclaringType.Assembly);
            }

            return Task.FromResult<IBinding>(new OpenApiBinding(context.Parameter, _document));
        }

        private class OpenApiBinding : IBinding
        {
            private ParameterInfo _parameter;
            private readonly OpenApiDocument _document;

            public OpenApiBinding(ParameterInfo parameter, OpenApiDocument document)
            {
                _parameter = parameter;
                _document = document;
            }

            public bool FromAttribute => false;

            public Task<IValueProvider> BindAsync(object value, ValueBindingContext context)
            {
                return Task.FromResult<IValueProvider>(new SimpleValueProvider(_parameter.ParameterType, value, _parameter.Name));
            }

            public Task<IValueProvider> BindAsync(BindingContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                return Task.FromResult<IValueProvider>(new SimpleValueProvider(_parameter.ParameterType, _document, _parameter.Name));
            }

            public ParameterDescriptor ToParameterDescriptor()
            {
                return new ParameterDescriptor
                {
                    Name = _parameter.Name,
                    DisplayHints = new ParameterDisplayHints
                    {
                        Description = _parameter.Name
                    }
                };
            }
        }
    }

    internal class SimpleValueProvider : IValueProvider
    {
        private readonly object _value;
        private readonly string _invokeString;

        public SimpleValueProvider(Type type, object value, string invokeString)
        {
            Type = type;
            _value = value;
            _invokeString = invokeString;
        }

        public Type Type { get; }

        public Task<object> GetValueAsync()
        {
            return Task.FromResult(_value);
        }

        public string ToInvokeString()
        {
            return _invokeString;
        }
    }
}