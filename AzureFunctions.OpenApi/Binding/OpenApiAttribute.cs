﻿using Microsoft.Azure.WebJobs.Description;
using System;

namespace AzureFunctions.OpenApi.Binding
{
    [AttributeUsage(AttributeTargets.Parameter)]
    [Binding]
    public class OpenApiAttribute : Attribute
    {
    }
}