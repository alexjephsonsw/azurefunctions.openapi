﻿using System;
using System.Net;

namespace AzureFunctions.OpenApi
{
    /// <summary>
    ///
    /// </summary>
    public class OpenApiResponsesAttribute : Attribute
    {
        /// <summary>
        ///
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        ///
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }
    }
}