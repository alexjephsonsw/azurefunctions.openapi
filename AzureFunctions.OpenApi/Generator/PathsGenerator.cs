﻿using Microsoft.OpenApi.Models;
using System;

namespace AzureFunctions.OpenApi.Generator
{
    internal class PathsGenerator : IPathsGenerator
    {
        private readonly IParametersGenerator _parametersGenerator;
        private readonly IMethodMapper _methodMapper;
        private readonly IOperationIdGenerator _operationIdGenerator;
        private readonly IResponsesGenerator _responsesGenerator;
        private readonly Lazy<IRouteGenerator> _routeGenerator;

        public PathsGenerator(IParametersGenerator parametersGenerator, IMethodMapper methodMapper, IOperationIdGenerator operationIdGenerator, IResponsesGenerator responsesGenerator, Lazy<IRouteGenerator> routeGenerator)
        {
            _parametersGenerator = parametersGenerator;
            _methodMapper = methodMapper;
            _operationIdGenerator = operationIdGenerator;
            _responsesGenerator = responsesGenerator;
            _routeGenerator = routeGenerator;
        }

        public OpenApiPaths Generate(FunctionCollection metaData)
        {
            var paths = new OpenApiPaths();
            foreach (var functionItem in metaData.Functions)
            {
                var function = functionItem.Value;

                var item = new OpenApiPathItem();

                var operation = new OpenApiOperation();
                operation.OperationId = _operationIdGenerator.Generate(function.Type, function.FunctionName);
                operation.Responses = _responsesGenerator.Generate(function.ResponseAttributes);
                operation.Parameters = _parametersGenerator.Generate(function);

                foreach (var method in function.HttpTriggerAttribute.Methods)
                {
                    item.AddOperation(_methodMapper.Map(method), operation);
                }

                var route = function.HttpTriggerAttribute.Route;
                if (route == null)
                {
                    var routeGenerator = _routeGenerator.Value;
                    route = routeGenerator.Generate(function.FunctionNameAttribute);
                }
                paths.Add(route, item);
            }

            return paths;
        }
    }
}