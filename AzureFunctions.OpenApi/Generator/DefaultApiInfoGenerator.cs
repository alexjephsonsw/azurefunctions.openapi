﻿using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace AzureFunctions.OpenApi.Generator
{
    internal class DefaultApiInfoGenerator : IApiInfoGenerator
    {
        private readonly IVersionGenerator _versionGenerator;
        private readonly IOptions<OpenApiOptions> _options;

        public DefaultApiInfoGenerator(IVersionGenerator versionGenerator, IOptions<OpenApiOptions> options)
        {
            _versionGenerator = versionGenerator;
            _options = options;
        }

        public OpenApiInfo Generate(FunctionCollection metaData)
        {
            var options = _options.Value;

            var apiInfo = new OpenApiInfo();
            apiInfo.Title = options.Title;
            apiInfo.Version = options.Version ?? _versionGenerator.Generate(metaData);
            apiInfo.Description = options.Description;
            return apiInfo;
        }
    }
}