﻿using Microsoft.OpenApi.Models;
using System;

namespace AzureFunctions.OpenApi.Generator
{
    internal class MethodMapper : IMethodMapper
    {
        public OperationType Map(string method)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            if (Enum.TryParse<OperationType>(method, ignoreCase: true, out var operationType))
            {
                return operationType;
            }

            switch (method.ToUpperInvariant())
            {
                case "DELETE":
                    return OperationType.Delete;

                case "GET":
                    return OperationType.Get;

                case "HEAD":
                    return OperationType.Head;

                case "OPTIONS":
                    return OperationType.Options;

                case "PATCH":
                    return OperationType.Patch;

                case "POST":
                    return OperationType.Post;

                case "PUT":
                    return OperationType.Put;

                case "TRACE":
                    return OperationType.Trace;

                default:
                    throw new ArgumentOutOfRangeException(nameof(method));
            }
        }
    }
}