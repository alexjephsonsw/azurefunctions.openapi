﻿using Microsoft.OpenApi.Models;
using System;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IParameterGenerator
    {
        OpenApiParameter Generate(ParameterInfo parameterInfo, string route);
    }
}