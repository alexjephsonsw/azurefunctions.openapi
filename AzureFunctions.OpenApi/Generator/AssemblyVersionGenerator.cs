﻿using AzureFunctions.OpenApi.Extensions;

namespace AzureFunctions.OpenApi.Generator
{
    internal class AssemblyVersionGenerator : IVersionGenerator
    {
        public string Generate(FunctionCollection metaData)
        {
            return metaData?.Assembly?.GetAssemblyVersion();
        }
    }
}