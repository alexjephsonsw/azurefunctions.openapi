﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Options;

namespace AzureFunctions.OpenApi.Generator
{
    internal class RouteGenerator : IRouteGenerator
    {
        private readonly IOptions<HttpOptions> _httpOptions;

        public RouteGenerator(IOptions<HttpOptions> httpOptions)
        {
            _httpOptions = httpOptions;
        }

        public string Generate(FunctionNameAttribute functionName)
        {
            var options = _httpOptions.Value;

            return options.RoutePrefix + functionName.Name;
        }
    }
}