﻿using Microsoft.Azure.WebJobs;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    public class Metadata
    {
        public readonly MethodInfo Function;

        public Metadata(MethodInfo function)
        {
            Function = function;
        }

        public Type Type { get; internal set; }
        public ParameterInfo HttpTriggerParameter { get; internal set; }
        public IEnumerable<ParameterInfo> OtherParameters { get; internal set; }
        public HttpTriggerAttribute HttpTriggerAttribute => HttpTriggerParameter.GetCustomAttribute<HttpTriggerAttribute>();
        public FunctionNameAttribute FunctionNameAttribute => Function.GetCustomAttribute<FunctionNameAttribute>();
        public string FunctionName => FunctionNameAttribute?.Name;
        public IEnumerable<OpenApiResponsesAttribute> ResponseAttributes => Function.GetCustomAttributes<OpenApiResponsesAttribute>();
    }
}