﻿using Microsoft.OpenApi.Models;
using System;

namespace AzureFunctions.OpenApi.Generator
{
    /// <summary>
    /// Generates sample content. The default implementation only supports application/json. To support other content types, replace <see cref="ISampleContentGenerator"/> with your own implementation.
    /// </summary>
    public interface ISampleContentGenerator
    {
        /// <summary>
        /// Generates sample content for <paramref name="contentType"/> and <paramref name="type"/>.
        /// </summary>
        /// <param name="contentType">The content type of the sample content.</param>
        /// <param name="type">The type of the model for an example to be generated.</param>
        /// <returns></returns>
        OpenApiMediaType Generate(string contentType, Type type);
    }
}