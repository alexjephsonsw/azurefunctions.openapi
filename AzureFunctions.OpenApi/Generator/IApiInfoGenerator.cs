﻿using Microsoft.OpenApi.Models;

namespace AzureFunctions.OpenApi.Generator
{
    /// <summary>
    /// Generates api information. The default implementation returns the server that this extension is running on. To generate your own api information, replace <see cref="IApiInfoGenerator"/> with your own implementation.
    /// </summary>
    public interface IApiInfoGenerator
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="metaData"></param>
        /// <returns></returns>
        OpenApiInfo Generate(FunctionCollection metaData);
    }
}