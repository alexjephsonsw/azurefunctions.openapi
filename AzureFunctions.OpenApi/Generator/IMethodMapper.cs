﻿using Microsoft.OpenApi.Models;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IMethodMapper
    {
        OperationType Map(string method);
    }
}