﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IResponsesGenerator
    {
        OpenApiResponses Generate(IEnumerable<OpenApiResponsesAttribute> responseAttributes);
    }
}