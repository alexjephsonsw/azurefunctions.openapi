﻿using System;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface ITypeIdGenerator
    {
        string Generate(Type parameterType);

        IDictionary<Type, string> GetTypes();
    }
}