﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal class DefaultServersGenerator : IServersGenerator
    {
        public IList<OpenApiServer> Generate(FunctionCollection metaData)
        {
            return new List<OpenApiServer> { new OpenApiServer { Url = "/" } };
        }
    }
}