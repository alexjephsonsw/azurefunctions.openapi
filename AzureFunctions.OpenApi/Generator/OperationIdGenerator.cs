﻿using System;

namespace AzureFunctions.OpenApi.Generator
{
    internal class OperationIdGenerator : IOperationIdGenerator
    {
        public string Generate(Type type, string functionName)
        {
            return $"{type.Name}.{functionName}";
        }
    }
}