﻿using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace AzureFunctions.OpenApi.Generator
{
    internal class ParametersGenerator : IParametersGenerator
    {
        private readonly ITypeIdGenerator _typeIdGenerator;

        private readonly IParameterGenerator _parameterGenerator;

        public ParametersGenerator(
            ITypeIdGenerator typeIdGenerator,
            IParameterGenerator parameterGenerator)
        {
            _typeIdGenerator = typeIdGenerator;
            _parameterGenerator = parameterGenerator;
        }

        public IList<OpenApiParameter> Generate(Metadata metadata)
        {
            var triggerParameterInfo = metadata.HttpTriggerParameter;

            var triggerParameter = new OpenApiParameter();
            if (triggerParameterInfo.ParameterType == typeof(HttpRequestMessage) || triggerParameterInfo.ParameterType == typeof(HttpRequest))
            {
                var httpBodySchema = new OpenApiSchema
                {
                    Type = "string",
                    Format = "binary",
                };
                triggerParameter.Name = "body";
                triggerParameter.Schema = httpBodySchema;
            }
            else
            {
                triggerParameter.Schema = new OpenApiSchema { Reference = new OpenApiReference { Id = _typeIdGenerator.Generate(triggerParameterInfo.ParameterType), Type = ReferenceType.Parameter } };
            }

            var list = new List<OpenApiParameter>() { triggerParameter };

            foreach (var parameterInfo in metadata.OtherParameters)
            {
                var parameter = _parameterGenerator.Generate(parameterInfo, metadata.HttpTriggerAttribute.Route);
                // this is a parameter that this can't we handle by default and no extensions provider support either
                if (parameter == null)
                {
                    continue;
                }

                list.Add(parameter);
            }

            return list;
        }
    }
}