﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal class TypeHandlerFactory : ITypeHandlerFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public TypeHandlerFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IEnumerable<ITypeHandler> Create()
        {
            return _serviceProvider.GetServices<ITypeHandler>();
        }
    }
}