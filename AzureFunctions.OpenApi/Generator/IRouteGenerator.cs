﻿using Microsoft.Azure.WebJobs;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IRouteGenerator
    {
        string Generate(FunctionNameAttribute functionName);
    }
}