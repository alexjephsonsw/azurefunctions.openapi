﻿using Microsoft.OpenApi.Models;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IPathsGenerator
    {
        OpenApiPaths Generate(FunctionCollection metaData);
    }
}