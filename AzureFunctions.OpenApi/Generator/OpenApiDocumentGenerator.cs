﻿using AzureFunctions.OpenApi.Extensions;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Schema;
using System;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal class OpenApiDocumentGenerator : IOpenApiDocumentGenerator
    {
        private readonly IMetadataGenerator _metadataGenerator;
        private readonly IApiInfoGenerator _apiInfoGenerator;
        private readonly IPathsGenerator _pathsGenerator;
        private readonly IServersGenerator _serversGenerator;
        private readonly ITagsGenerator _tagsGenerator;
        private readonly ISecurityGenerator _securityGenerator;
        private readonly ITypeIdGenerator _typeIdGenerator;

        public OpenApiDocumentGenerator(IMetadataGenerator metadataGenerator,
                                        IApiInfoGenerator apiInfoGenerator,
                                        IPathsGenerator pathsGenerator,
                                        IServersGenerator serversGenerator,
                                        ITagsGenerator tagsGenerator,
                                        ISecurityGenerator securityGenerator,
                                        ITypeIdGenerator typeIdGenerator)
        {
            _metadataGenerator = metadataGenerator;
            _apiInfoGenerator = apiInfoGenerator;
            _pathsGenerator = pathsGenerator;
            _serversGenerator = serversGenerator;
            _tagsGenerator = tagsGenerator;
            _securityGenerator = securityGenerator;
            _typeIdGenerator = typeIdGenerator;
        }

        public OpenApiDocument Generate(Assembly assembly)
        {
            var metaData = _metadataGenerator.Generate(assembly);

            var document = new OpenApiDocument();
            document.Paths = _pathsGenerator.Generate(metaData);
            document.Info = _apiInfoGenerator.Generate(metaData);
            document.Servers = _serversGenerator.Generate(metaData);
            document.Tags = _tagsGenerator.Generate(metaData);
            document.SecurityRequirements = _securityGenerator.Generate(metaData);

            AddSchemas(document);

            return document;
        }

        private void AddSchemas(OpenApiDocument document)
        {
            foreach (var type in _typeIdGenerator.GetTypes())
            {
                var schema = CreateSchema(type.Key);
                document.Components.Schemas.Add(type.Value, new OpenApiSchema { Type = "object", })
            }
        }

        private OpenApiSchema CreateSchema(Type type)
        {
            var schemaGenerator = new JsonSchemaGenerator();
            var schema = schemaGenerator.Generate(type);

            return schema.ToOpenApiSchema(type);

            //switch (schema.Type)
            //{
            //    case JsonSchemaType.None:
            //        break;
            //    case JsonSchemaType.String:
            //        return new OpenApiSchema { Type = "string", Required = schema.re };
            //        break;
            //    case JsonSchemaType.Float:
            //        break;
            //    case JsonSchemaType.Integer:
            //        break;
            //    case JsonSchemaType.Boolean:
            //        break;
            //    case JsonSchemaType.Object:
            //        break;
            //    case JsonSchemaType.Array:
            //        break;
            //    case JsonSchemaType.Null:
            //        break;
            //    case JsonSchemaType.Any:
            //        break;
            //    default:
            //        throw new ArgumentOutOfRangeException(nameof(schema.Type));
            //}

            //return new OpenApiSchema()
            //{
            //};
        }
    }
}