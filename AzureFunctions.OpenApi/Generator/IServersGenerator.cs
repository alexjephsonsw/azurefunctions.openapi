﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    /// <summary>
    /// Generates a list of servers. The default implementation returns the server that this extension is running on. To generate your own list, replace <see cref="IServersGenerator"/> with your own implementation.
    /// </summary>
    public interface IServersGenerator
    {
        /// <summary>
        /// Generates a list of servers that this documentation applies to
        /// </summary>
        /// <param name="metaData"></param>
        /// <returns></returns>
        IList<OpenApiServer> Generate(FunctionCollection metaData);
    }
}