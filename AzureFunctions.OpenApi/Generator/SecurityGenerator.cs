﻿using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal class SecurityGenerator : ISecurityGenerator
    {
        private readonly IEnumerable<SecurityRequirement> _requirements;

        public SecurityGenerator(IEnumerable<SecurityRequirement> requirements)
        {
            _requirements = requirements;
        }

        public IList<OpenApiSecurityRequirement> Generate(FunctionCollection metaData)
        {
            var ret = new List<OpenApiSecurityRequirement>();
            var item = new OpenApiSecurityRequirement();
            foreach (var requirement in _requirements)
            {
                var scheme = new OpenApiSecurityScheme();
                scheme.In = requirement.ParameterLocation;
                scheme.Name = requirement.ParameterName;
                scheme.Scheme = requirement.Scheme;
                scheme.Type = requirement.Type;
                scheme.OpenIdConnectUrl = requirement.OpenIdConnectUrl;

                item.Add(scheme, new List<string>());
            }

            ret.Add(item);

            return ret;
        }
    }
}