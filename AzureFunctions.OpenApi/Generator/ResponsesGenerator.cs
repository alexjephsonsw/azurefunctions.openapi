﻿using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal class ResponsesGenerator : IResponsesGenerator
    {
        private readonly ITypeIdGenerator _typeIdGenerator;
        private readonly ISampleContentGenerator _sampleContentGenerator;
        private readonly IOptions<OpenApiOptions> _options;

        public ResponsesGenerator(ITypeIdGenerator typeIdGenerator, ISampleContentGenerator sampleContentGenerator, IOptions<OpenApiOptions> options)
        {
            _typeIdGenerator = typeIdGenerator;
            _sampleContentGenerator = sampleContentGenerator;
            _options = options;
        }

        public OpenApiResponses Generate(IEnumerable<OpenApiResponsesAttribute> responseAttributes)
        {
            var responses = new OpenApiResponses();
            var contentTypes = _options.Value.ContentTypes;
            foreach (var responseAttribute in responseAttributes)
            {
                var response = new OpenApiResponse();
                if (responseAttribute.Type != null)
                {
                    response.Reference = new OpenApiReference { Id = _typeIdGenerator.Generate(responseAttribute.Type), Type = ReferenceType.Response };
                    foreach (var contentType in contentTypes)
                    {
                        var sampleContent = _sampleContentGenerator.Generate(contentType, responseAttribute.Type);
                        if (sampleContent == null)
                        {
                            continue;
                        }
                        response.Content.Add(contentType, sampleContent);
                    }
                }
                else
                {
                    foreach (var contentType in contentTypes)
                    {
                        response.Content.Add(contentType, new OpenApiMediaType { Example = new RawString(responseAttribute.StatusCode.ToString()) });
                    }
                }

                responses.Add(((int)responseAttribute.StatusCode).ToString(), response);
            }

            return responses;
        }
    }
}