﻿using Microsoft.OpenApi;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Writers;

namespace AzureFunctions.OpenApi.Generator
{
    internal class RawString : IOpenApiAny
    {
        private readonly string _value;

        public RawString(string value)
        {
            _value = value;
        }

        public AnyType AnyType => AnyType.Primitive;

        public void Write(IOpenApiWriter writer, OpenApiSpecVersion specVersion)
        {
            writer.WriteRaw(_value);
        }
    }
}