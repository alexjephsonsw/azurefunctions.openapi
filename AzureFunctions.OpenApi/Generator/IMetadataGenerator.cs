﻿using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IMetadataGenerator
    {
        FunctionCollection Generate(Assembly assembly);
    }
}