﻿namespace AzureFunctions.OpenApi.Generator
{
    public interface IVersionGenerator
    {
        /// <summary>
        /// Generates a version number. The default implementation reads the version from the Assembly. To generate your own version, replace <see cref="IVersionGenerator"/> with your own implementation.
        /// </summary>
        /// <param name="metaData"></param>
        /// <returns>The version represented as a string</returns>
        string Generate(FunctionCollection metaData);
    }
}