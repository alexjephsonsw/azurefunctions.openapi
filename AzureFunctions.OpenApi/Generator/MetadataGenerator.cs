﻿using AzureFunctions.OpenApi.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal class MetadataGenerator : IMetadataGenerator
    {
        public FunctionCollection Generate(Assembly assembly)
        {
            var functions = assembly.GetHttpTriggerMethods().ToList();
            if (functions.Count == 0)
            {
                return null;
            }

            var collection = new FunctionCollection
            {
                Assembly = assembly,
                Functions = new Dictionary<string, Metadata>()
            };

            foreach (var function in functions)
            {
                var metadata = new Metadata(function)
                {
                    HttpTriggerParameter = function.GetHttpTriggerParameter(),
                    OtherParameters = function.GetOtherParameters(),
                    Type = function.DeclaringType
                };
                collection.Functions.Add($"{function.DeclaringType.Name}-{function.Name}", metadata);
            }

            return collection;
        }
    }
}