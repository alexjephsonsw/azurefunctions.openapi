﻿using AzureFunctions.OpenApi.Extensions;
using Microsoft.OpenApi.Models;
using System;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal class ParameterGenerator : IParameterGenerator
    {
        private readonly ITypeIdGenerator _typeIdGenerator;
        private readonly ITypeHandlerFactory _typeHandlerFactory;

        public ParameterGenerator(ITypeIdGenerator typeIdGenerator, ITypeHandlerFactory typeHandlerFactory)
        {
            _typeIdGenerator = typeIdGenerator;
            _typeHandlerFactory = typeHandlerFactory;
        }

        public OpenApiParameter Generate(ParameterInfo parameterInfo, string routeTemplate)
        {
            var handlers = _typeHandlerFactory.Create();
            foreach (var typeHandler in handlers)
            {
                var parameterData = typeHandler.Handle(parameterInfo, routeTemplate);
                if (parameterData == null)
                {
                    continue;
                }

                var parameter = new OpenApiParameter
                {
                    Schema = CreateSchema(parameterData.Type),
                    Name = parameterData.Name,
                    Required = parameterData.Required,
                    In = parameterData.ParameterLocation
                };

                return parameter;
            }

            return null;
        }

        private OpenApiReference CreateReference(Type type)
        {
            if (type.IsSimpleType())
            {
                return null;
            }
            return new OpenApiReference { Id = _typeIdGenerator.Generate(type), Type = ReferenceType.Parameter };
        }

        private OpenApiSchema CreateSchema(Type type)
        {
            if (type.IsSimpleType())
            {
                return new OpenApiSchema { Type = type.ToJsonSchema() };
            }
            else
            {
                return new OpenApiSchema { Reference = CreateReference(type) };
            }
        }
    }
}