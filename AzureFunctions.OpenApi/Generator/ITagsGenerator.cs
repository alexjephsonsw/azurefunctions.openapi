﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    /// <summary>
    /// Generates a list of tags. The default implementation returns an empty list. To generate your own list, replace <see cref="ITagsGenerator"/> with your own implementation.
    /// </summary>
    public interface ITagsGenerator
    {
        IList<OpenApiTag> Generate(FunctionCollection metaData);
    }
}