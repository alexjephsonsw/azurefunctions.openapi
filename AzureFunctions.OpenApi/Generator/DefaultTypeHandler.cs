﻿using Microsoft.AspNetCore.Routing.Template;
using Microsoft.Azure.WebJobs.Description;
using Microsoft.OpenApi.Models;
using System;
using System.Linq;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal class DefaultTypeHandler : ITypeHandler
    {
        public ParameterData Handle(ParameterInfo parameterInfo, string route)
        {
            foreach (var attribute in parameterInfo.GetCustomAttributes())
            {
                // we skip any parameters that have binding attributes on them
                // bound parameters must be handled using a custom type handler
                if (attribute.GetType().GetCustomAttribute<BindingAttribute>() != null)
                {
                    return null;
                }
            }

            var routeTemplate = TemplateParser.Parse(route);
            if (routeTemplate.Parameters.FirstOrDefault(s => s.Name.Equals(parameterInfo.Name, StringComparison.InvariantCultureIgnoreCase)) != null)
            {
                return new ParameterData
                {
                    Name = parameterInfo.Name,
                    ParameterLocation = ParameterLocation.Path,
                    Required = true,
                    Type = parameterInfo.ParameterType,
                    Style = ParameterStyle.Simple
                };
            }

            return null;
        }
    }

    public enum ParameterLocatoin : int
    {
    }
}