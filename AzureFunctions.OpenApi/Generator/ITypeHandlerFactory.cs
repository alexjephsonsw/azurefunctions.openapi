﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface ITypeHandlerFactory
    {
        IEnumerable<ITypeHandler> Create();
    }
}