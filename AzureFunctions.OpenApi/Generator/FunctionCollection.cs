﻿using System.Collections.Generic;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    public class FunctionCollection
    {
        internal Assembly Assembly { get; set; }

        /// <summary>
        ///
        /// </summary>
        public IDictionary<string, Metadata> Functions { get; set; } = new Dictionary<string, Metadata>();
    }
}