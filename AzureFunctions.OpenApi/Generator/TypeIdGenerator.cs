﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace AzureFunctions.OpenApi.Generator
{
    internal class TypeIdGenerator : ITypeIdGenerator
    {
        private readonly ConcurrentDictionary<Type, string> _parameters = new ConcurrentDictionary<Type, string>();

        public string Generate(Type parameterType)
        {
            if (parameterType == null)
            {
                throw new ArgumentNullException(nameof(parameterType));
            }

            if (_parameters.ContainsKey(parameterType))
            {
                return _parameters[parameterType];
            }
            else
            {
                _parameters.TryAdd(parameterType, parameterType.Name);

                return parameterType.Name;
            }
        }

        public IEnumerable<Type> GetTypes()
        {
            return _parameters.Keys;
        }
    }
}