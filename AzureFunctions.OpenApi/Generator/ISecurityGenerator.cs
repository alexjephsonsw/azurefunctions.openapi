﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface ISecurityGenerator
    {
        IList<OpenApiSecurityRequirement> Generate(FunctionCollection metaData);
    }
}