﻿using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    /// <summary>
    ///
    /// </summary>
    public interface ITypeHandler
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="parameterInfo"></param>
        /// <returns></returns>
        ParameterData Handle(ParameterInfo parameterInfo, string routeTemplate);
    }
}