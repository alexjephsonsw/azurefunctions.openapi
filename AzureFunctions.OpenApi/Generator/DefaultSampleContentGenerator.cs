﻿using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    /// <summary>
    /// The default sample content generator. Only supports JSON content types. To support other content types, replace ISampleContentGenerator with your own implementation
    /// </summary>
    internal class DefaultSampleContentGenerator : ISampleContentGenerator
    {
        public OpenApiMediaType Generate(string contentType, Type type)
        {
            var obj = BuildSample(type);
            switch (contentType.ToLowerInvariant())
            {
                case "application/json":
                case "text/json": // ??????
                    return new OpenApiMediaType { Example = new RawString(JsonConvert.SerializeObject(obj)) };

                default:
                    return null;
            }
        }

        private object BuildSample(Type type)
        {
            var instance = Activator.CreateInstance(type);
            foreach (var property in type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty))
            {
                property.SetValue(instance, GenerateDefaultValue(property.PropertyType));
            }

            return instance;
        }

        private object GenerateDefaultValue(Type propertyType)
        {
            if (propertyType == typeof(string))
            {
                return "string";
            }
            else if (propertyType.IsValueType && Nullable.GetUnderlyingType(propertyType) == null)
            {
                return Activator.CreateInstance(propertyType);
            }
            else
            {
                return null;
            }
        }
    }
}