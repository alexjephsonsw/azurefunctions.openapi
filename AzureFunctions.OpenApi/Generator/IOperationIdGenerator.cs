﻿using System;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IOperationIdGenerator
    {
        string Generate(Type type, string functionName);
    }
}