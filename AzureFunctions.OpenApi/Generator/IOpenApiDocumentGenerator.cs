﻿using Microsoft.OpenApi.Models;
using System.Reflection;

namespace AzureFunctions.OpenApi.Generator
{
    internal interface IOpenApiDocumentGenerator
    {
        OpenApiDocument Generate(Assembly assembly);
    }
}