﻿using Microsoft.OpenApi.Models;
using System;

namespace AzureFunctions.OpenApi
{
    /// <summary>
    ///
    /// </summary>
    public class SecurityRequirement
    {
        /// <summary>
        ///
        /// </summary>
        public ParameterLocation ParameterLocation { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public string ParameterName { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public string Scheme { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public SecuritySchemeType Type { get; internal set; }

        /// <summary>
        ///
        /// </summary>
        public Uri OpenIdConnectUrl { get; internal set; }
    }
}