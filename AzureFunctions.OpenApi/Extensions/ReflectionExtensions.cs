﻿using AzureFunctions.OpenApi.Binding;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AzureFunctions.OpenApi.Extensions
{
    internal static class ReflectionExtensions
    {
        private static readonly Type _iEnumerableOpenGenericType = typeof(IEnumerable<>);
        private static readonly Type _nullableOpenGenericType = typeof(Nullable<>);
        private static readonly Type _timespanType = typeof(TimeSpan);
        private static readonly Type _dateTimeOffsetType = typeof(DateTimeOffset);
        private static readonly Type _guidType = typeof(Guid);

        public static ParameterInfo GetHttpTriggerParameter(this MethodInfo method)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            return method.GetParameters().FirstOrDefault(q => q.ExistsCustomAttribute<HttpTriggerAttribute>());
        }

        public static IEnumerable<ParameterInfo> GetOtherParameters(this MethodInfo method)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            return method.GetParameters().Where(q => !q.ExistsCustomAttribute<HttpTriggerAttribute>());
        }

        public static bool ExistsCustomAttribute<T>(this ParameterInfo element, bool inherit = false) where T : Attribute
        {
            if (element == null)
            {
                return false;
            }

            return element.GetCustomAttribute<T>(inherit) != null;
        }

        public static bool ExistsCustomAttribute<T>(this MemberInfo element, bool inherit = false) where T : Attribute
        {
            if (element == null)
            {
                return false;
            }

            return element.GetCustomAttribute<T>(inherit) != null;
        }

        public static IEnumerable<MethodInfo> GetHttpTriggerMethods(this Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }

            return assembly
                .GetTypes()
                .SelectMany(p => p.GetMethods())
                .Where(p => p.ExistsCustomAttribute<FunctionNameAttribute>())
                .Where(p => !p.ExistsCustomAttribute<OpenApiIgnoreAttribute>())
                .Where(p => p.GetParameters().FirstOrDefault(q => q.ExistsCustomAttribute<HttpTriggerAttribute>()) != null)
                .Where(p => p.GetParameters().FirstOrDefault(q => q.ExistsCustomAttribute<OpenApiAttribute>()) == null);
        }

        public static string GetAssemblyVersion(this Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }

            return assembly.GetName()?.Version?.ToString();
        }

        public static bool IsNullableType(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == _nullableOpenGenericType;
        }

        public static bool IsEnumerableType(this Type type)
        {
            if (!type.IsGenericType)
            {
                return false;
            }
            return type.GetGenericTypeDefinition() == _iEnumerableOpenGenericType;
        }

        public static bool IsSimpleType(this Type type)
        {
            if (type.IsNullableType())
            {
                type = type.GetGenericArguments()[0];
            }

            if (type.IsEnum)
            {
                return true;
            }

            if (type == _guidType)
            {
                return true;
            }

            var tc = Type.GetTypeCode(type);
            switch (tc)
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                case TypeCode.Char:
                case TypeCode.String:
                case TypeCode.Boolean:
                case TypeCode.DateTime:
                    return true;

                case TypeCode.Object:
                    return (_timespanType == type) || (_dateTimeOffsetType == type);

                default:
                    return false;
            }
        }

        public static string ToJsonSchema(this Type type)
        {
            var gen = new JsonSchemaGenerator();
            var schema = gen.Generate(type);
            return schema.Type.Value.ToString();
        }
    }
}