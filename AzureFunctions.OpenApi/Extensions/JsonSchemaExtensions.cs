﻿using AzureFunctions.OpenApi.Generator;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureFunctions.OpenApi.Extensions
{
    public static class JsonSchemaExtensions
    {
        public static OpenApiSchema ToOpenApiSchema(this JsonSchema schema)
        {
            Microsoft.OpenApi.Rea
            var openApiSchema = new OpenApiSchema();

            openApiSchema.AdditionalProperties = schema.AdditionalProperties.ToOpenApiSchema();
            openApiSchema.AdditionalPropertiesAllowed = schema.AllowAdditionalProperties;
            //openApiSchema.AllOf = ??
            //openApiSchema.AnyOf = ??
            //openApiSchema.Default = schema.Default;
            openApiSchema.Description = schema.Description;
            //openApiSchema.Discriminator = ??
            openApiSchema.Enum = schema.Enum.Select(x => (IOpenApiAny)new RawString(x.ToString())).ToList();
            openApiSchema.
        }
    }
}