﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace AzureFunctions.OpenApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddOpenApi(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection Replace<TService, TImplementation>(this IServiceCollection services, ServiceLifetime? serviceLifetime = null) where TImplementation : TService
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            var type = typeof(TService);
            var foundServices = services.Where(x => x.ServiceType == type);
            if (!foundServices.Any())
            {
                throw new InvalidOperationException($"There is no service registered with a type of {type.Name}.");
            }

            if (foundServices.Count() > 1)
            {
                throw new InvalidOperationException($"There is more than once service registered with a type of {type.Name}. You can only replace single items");
            }

            var service = foundServices.Single();

            services.Remove(service);

            services.Add(new ServiceDescriptor(service.ServiceType, typeof(TImplementation), serviceLifetime ?? service.Lifetime));

            return services;
        }
    }
}